import scrapy
import re
import jdatetime


class Exchange(scrapy.Spider):
    name = "exchange"

    start_urls = [
        "https://www.rouydad24.ir/fa/search/1/-1/-1/10/%D9%82%DB%8C%D9%85%D8%AA%20%D8%AF%D9%84%D8%A7%D8%B1?from=1380/01/01&to=1399/01/14"
    ]

    def parse(self, response):
        links = response.css("a.tag_title")
        for link in links:
            text = link.css("::text").get()
            text = text.replace(" ","")
            match = re.search("([۰۱۲۳۴۵۶۷۸۹]{1,2}\/[۰۱۲۳۴۵۶۷۸۹]{1,2}\/[۰۱۲۳۴۵۶۷۸۹]{1,2})", text)
            if match:
                try:
                    href = response.urljoin(link.css("::attr(href)").get())
                    persian_date = match.group()
                    persian_date = persian_date.replace("۰","0").replace("۱","1").replace("۲","2").replace("۳","3").replace("۴","4").replace("۵","5").replace("۶","6").replace("۷","7").replace("۸","8").replace("۹","9")
                    persian_date_sep = persian_date.split("/")
                    jyear = persian_date_sep[0]
                    if len(jyear)==2:
                        jyear = "13"+jyear
                    jmonth=persian_date_sep[1]
                    jday=persian_date_sep[2]
                    gd = jdatetime.JalaliToGregorian(int(jyear),int(jmonth),int(jday))
                    yield {
                        "date":f"{gd.gyear}/{gd.gmonth}/{gd.gday}",
                        "jalali":persian_date,
                        "href":href,
                    }
                except:
                    pass
            
        next_href = response.css("div.pagination a[title='صفحه قبل']::attr(href)").get()
        if next_href is not None:
            yield response.follow(next_href, callback=self.parse)